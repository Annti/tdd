package annti.test;

import annti.Filters;
import annti.exceptions.NumberTwoFoundException;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;

import static annti.test.FiltersTest.expected;
import static org.junit.Assert.fail;

/**
 * Created by aneta on 08.05.16.
 */
@RunWith(DataProviderRunner.class)
public class FiltersTestDataProvider {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @DataProvider(format = "%m - %p[0]")
    public static Object[][] numbersData() {
        return new Object[][]{
                {
                        "basic",
                        "3 ,4, 5, sernik",
                        new ArrayList<>(Arrays.asList(3, 4, 5))
                },
                {
                        "empty",
                        "",
                        new ArrayList<>()
                }
        };
    }

    @Test
    @UseDataProvider("numbersData")
    public void numbers(final String name, final String in, final ArrayList<Integer> expected) throws Exception {

        //Act
        ArrayList<Integer> out = Filters.numbers(in);

        //Assert
        Assert.assertEquals(expected, out);
    }

    @DataProvider(format = "%m - %p[0]")
    public static Object[][] twoRemoverData() {
        return new Object[][]{
                {
                        "basic",
                        "3",
                        3,
                        null
                },
                {
                        "two",
                        "2",
                        0,
                        NumberTwoFoundException.class
                },
                {
                        "empty",
                        "",
                        0,
                        null
                }
        };
    }

    @Test
    @UseDataProvider("twoRemoverData")
    public void twoRemover(final String name, final String in, final int expected,
                           final Class<? extends Exception> exception) throws Exception, NumberTwoFoundException {
        //Arrange
        if(exception != null){
            thrown.expect(exception);
        }

        //Act
        int out = Filters.twoRemover(in);

        //Assert
        Assert.assertEquals(expected, out);


    }

}