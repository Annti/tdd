package annti.test;

import annti.Filters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.IntStream;

/**
 * Created by aneta on 08.05.16.
 */

@RunWith(Parameterized.class)
public class FiltersTest {
    @Parameterized.Parameter
    public static String name;
    @Parameterized.Parameter(value = 1)
    public static String in;
    @Parameterized.Parameter(value = 2)
    public static ArrayList<Integer> expected;

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                        {
                                "numbers_basic",
                                "3 ,4, 5, sernik",
                                new ArrayList<>(Arrays.asList(3, 4, 5))
                        },
                        {
                                "numbers_empty",
                                "",
                                new ArrayList<>()

                        }
                }
        );
    }


    @Test
    public void numbersBasic() throws Exception {

        //Act
        ArrayList<Integer> out = Filters.numbers(in);

        //Assert
        Assert.assertEquals(expected, out);
    }

}